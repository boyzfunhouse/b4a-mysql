﻿Type=Activity
Version=5.02
ModulesStructureVersion=1
B4A=true
@EndOfDesignText@
#Region  Activity Attributes 
	#FullScreen: False
	#IncludeTitle: True
#End Region

Sub Process_Globals
	'These global variables will be declared once when the application starts.
	'These variables can be accessed from all modules.
	Private SUBJECTS_LIST = "subjects_list" As String
	Private SUBJECT_CREDIT = "subject_credit" As String
	Private SUBJECT_ADD = "subject_add" As String
	Private SUBJECT_EDIT = "subject_edit" As String
	Private SUBJECT_DELETE = "subject_delete" As String
End Sub

Sub Globals
	'These global variables will be redeclared each time the activity is created.
	'These variables can only be accessed from this module.

	Private ListView1 As ListView
	Private btnCancel As Button
	Private btnSave As Button
	Private txtCredit As EditText
	Private txtSubject As EditText
	
	Private ID As String
	Private NEW_ID As String
End Sub

Sub Activity_Create(FirstTime As Boolean)
	'Do not forget to load the layout file created with the visual designer. For example:
	Activity.LoadLayout("Subject")
	FetchSubjectsList
End Sub

Sub Activity_Resume

End Sub

Sub Activity_Pause (UserClosed As Boolean)

End Sub

Sub FetchSubjectsList
	ProgressDialogShow("Fetching list of subjects")
	'Gets all the available countries
	ExecuteRemoteQuery("SELECT title, id FROM subjects ORDER BY id", SUBJECTS_LIST)
End Sub

Sub ExecuteRemoteQuery(Query As String, JobName As String)
	Dim job As HttpJob
	job.Initialize(JobName, Me)
'	job.PostString("http://www.basic4ppc.com/android/countries.php", Query)
	job.PostString("http://www.boonsanti.com/b4a.php", Query)
End Sub

Sub JobDone(Job As HttpJob)
	ProgressDialogHide
	If Job.Success Then
	Dim res As String
		res = Job.GetString
		Log("Response from server: " & res)
		Dim parser As JSONParser
		parser.Initialize(res)
		Select Job.JobName
			Case SUBJECTS_LIST
				ListView1.Clear
				NEW_ID = "01"
				Dim SUBJECTS As List
				SUBJECTS = parser.NextArray 'returns a list with maps
				For i = 0 To SUBJECTS.Size - 1
					Dim m As Map
					m = SUBJECTS.Get(i)
					'We are using a custom type named TwoLines (declared in Sub Globals).
					'It allows us to later get the two values when the user presses on an item.
					Dim tl As TwoLines
					tl.First = m.Get("id")
					tl.Second = m.Get("title")
					ListView1.AddTwoLines2(tl.First, tl.Second, tl)
					If IsNumber(tl.First) Then
						NEW_ID = NumberFormat(tl.First+1, 2, 0)
					End If
				Next
			Case SUBJECT_CREDIT
				Dim l As List
				l = parser.NextArray
				If l.Size = 0 Then
					txtCredit.Text = ""
				Else
					Dim m As Map
					m = l.Get(0)
					txtCredit.Text = m.Get("credit")
				End If
			Case SUBJECT_ADD
				ClearSubject
				ToastMessageShow("Subject added.", False)
				FetchSubjectsList
			Case SUBJECT_EDIT
				ClearSubject
				ToastMessageShow("Subject updated.", False)
				FetchSubjectsList
			Case SUBJECT_DELETE
				ClearSubject
				ToastMessageShow("Subject deleted.", False)
				FetchSubjectsList
		End Select
	Else
		ToastMessageShow("Error: " & Job.ErrorMessage, True)
	End If
	Job.Release
End Sub

Sub ListView1_ItemClick (Position As Int, Value As Object)
	Dim tl As TwoLines
	tl = Value
	ID = tl.First
	txtSubject.Text = tl.Second
	txtCredit.Text = ""
	ExecuteRemoteQuery("SELECT credit FROM subjects WHERE id='" & tl.First & "'", _
		SUBJECT_CREDIT)
End Sub
Sub btnSave_Click
	If ID = "" Then
		ExecuteRemoteQuery("INSERT INTO subjects (id, title, credit) VALUES ('" & NEW_ID & "','" & txtSubject.Text & "'," & txtCredit.Text & ")", _
		SUBJECT_ADD)
	Else
		ExecuteRemoteQuery("UPDATE subjects SET title='" & txtSubject.Text & "', credit=" & txtCredit.Text & " WHERE id='" & ID & "'", _
		SUBJECT_EDIT)
	End If
	
End Sub

Sub btnCancel_Click
	ClearSubject
End Sub

Sub ClearSubject
	ID = ""
	txtCredit.Text = ""
	txtSubject.Text = ""
End Sub

Sub btnDelete_Click
	If ID <> "" Then
		ExecuteRemoteQuery("DELETE FROM subjects WHERE id='" & ID & "'", _
		SUBJECT_DELETE)
	End If
End Sub